\contentsline {section}{\numberline {1}Business Rules}{4}{section.1}%
\contentsline {section}{\numberline {2}Database Design}{5}{section.2}%
\contentsline {subsection}{\numberline {2.1}Database Diagram}{5}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Changes from Original Model}{6}{subsection.2.2}%
\contentsline {subsubsection}{\numberline {2.2.1}Added Tables}{6}{subsubsection.2.2.1}%
\contentsline {subsubsection}{\numberline {2.2.2}Added Views}{6}{subsubsection.2.2.2}%
\contentsline {subsubsection}{\numberline {2.2.3}Added/Removed Columns}{6}{subsubsection.2.2.3}%
\contentsline {section}{\numberline {3}App Description}{7}{section.3}%
\contentsline {subsection}{\numberline {3.1}Database}{7}{subsection.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}Database.java}{7}{subsubsection.3.1.1}%
\contentsline {subsection}{\numberline {3.2}Search}{7}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}Search.java}{7}{subsubsection.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2}SearchQuery.java}{7}{subsubsection.3.2.2}%
\contentsline {subsection}{\numberline {3.3}Admin}{7}{subsection.3.3}%
\contentsline {subsubsection}{\numberline {3.3.1}CancellationRequest.java}{7}{subsubsection.3.3.1}%
\contentsline {subsubsection}{\numberline {3.3.2}DeleteApartmentComplex.java}{7}{subsubsection.3.3.2}%
\contentsline {subsection}{\numberline {3.4}Webserver}{8}{subsection.3.4}%
\contentsline {subsubsection}{\numberline {3.4.1}Server.java}{8}{subsubsection.3.4.1}%
\contentsline {subsubsection}{\numberline {3.4.2}CustomHttpHandler.java}{8}{subsubsection.3.4.2}%
\contentsline {subsubsection}{\numberline {3.4.3}RequestHandler.java}{8}{subsubsection.3.4.3}%
\contentsline {subsubsection}{\numberline {3.4.4}PagePerm.java}{8}{subsubsection.3.4.4}%
\contentsline {subsubsection}{\numberline {3.4.5}UserCookie.java}{8}{subsubsection.3.4.5}%
\contentsline {subsubsection}{\numberline {3.4.6}UserHandler.java}{9}{subsubsection.3.4.6}%
\contentsline {paragraph}{Potential values for data:}{9}{section*.2}%
\contentsline {subsubsection}{\numberline {3.4.7}LoginHandler.java}{9}{subsubsection.3.4.7}%
\contentsline {subsubsection}{\numberline {3.4.8}CreateUserHandler.java}{9}{subsubsection.3.4.8}%
\contentsline {subsubsection}{\numberline {3.4.9}ApartmentHandler.java}{9}{subsubsection.3.4.9}%
\contentsline {subsubsection}{\numberline {3.4.10}AdminCancelRequest.java}{9}{subsubsection.3.4.10}%
\contentsline {subsubsection}{\numberline {3.4.11}AdminGetUserData.java}{9}{subsubsection.3.4.11}%
\contentsline {subsubsection}{\numberline {3.4.12}AdminRentalRequestHandler.java}{9}{subsubsection.3.4.12}%
\contentsline {subsubsection}{\numberline {3.4.13}ApartmentViewHandler.html}{10}{subsubsection.3.4.13}%
\contentsline {subsubsection}{\numberline {3.4.14}CencelRequestHandler.java}{10}{subsubsection.3.4.14}%
\contentsline {subsubsection}{\numberline {3.4.15}CancelRequestStatusHandler.java}{10}{subsubsection.3.4.15}%
\contentsline {subsubsection}{\numberline {3.4.16}ComplexHandler}{10}{subsubsection.3.4.16}%
\contentsline {subsubsection}{\numberline {3.4.17}ContractHandler.java}{10}{subsubsection.3.4.17}%
\contentsline {subsubsection}{\numberline {3.4.18}CreateApartmentHandler.java}{10}{subsubsection.3.4.18}%
\contentsline {subsubsection}{\numberline {3.4.19}CreateComplexHandler.java}{10}{subsubsection.3.4.19}%
\contentsline {subsubsection}{\numberline {3.4.20}CreateReviewHandler.java}{10}{subsubsection.3.4.20}%
\contentsline {subsubsection}{\numberline {3.4.21}CreateUserHandler.java}{10}{subsubsection.3.4.21}%
\contentsline {subsubsection}{\numberline {3.4.22}GetBillsHandler.java}{11}{subsubsection.3.4.22}%
\contentsline {subsubsection}{\numberline {3.4.23}GetReviewHandler.java}{11}{subsubsection.3.4.23}%
\contentsline {subsubsection}{\numberline {3.4.24}LateFeeHandler.java}{11}{subsubsection.3.4.24}%
\contentsline {subsubsection}{\numberline {3.4.25}PoPalHandler.java}{11}{subsubsection.3.4.25}%
\contentsline {subsubsection}{\numberline {3.4.26}RecommendationHandler.java}{11}{subsubsection.3.4.26}%
\contentsline {subsubsection}{\numberline {3.4.27}RentalInfoHandler.java}{11}{subsubsection.3.4.27}%
\contentsline {subsubsection}{\numberline {3.4.28}RentalRequestHandler.java}{11}{subsubsection.3.4.28}%
\contentsline {subsubsection}{\numberline {3.4.29}SearchResultsHandler.java}{11}{subsubsection.3.4.29}%
\contentsline {subsubsection}{\numberline {3.4.30}UserContractInfoHandlerjava}{11}{subsubsection.3.4.30}%
\contentsline {subsection}{\numberline {3.5}Login}{12}{subsection.3.5}%
\contentsline {subsubsection}{\numberline {3.5.1}CreateAccount.java}{12}{subsubsection.3.5.1}%
\contentsline {subsubsection}{\numberline {3.5.2}PasswordSecurity.java}{12}{subsubsection.3.5.2}%
\contentsline {subsubsection}{\numberline {3.5.3}Login.java}{12}{subsubsection.3.5.3}%
\contentsline {subsection}{\numberline {3.6}Recommendations}{12}{subsection.3.6}%
\contentsline {subsubsection}{\numberline {3.6.1}Recommendations.java}{12}{subsubsection.3.6.1}%
\contentsline {subsubsection}{\numberline {3.6.2}Review.java}{12}{subsubsection.3.6.2}%
\contentsline {section}{\numberline {4}User Manual}{13}{section.4}%
\contentsline {subsection}{\numberline {4.1}Getting Started}{13}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Creating a User Account}{13}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Login}{14}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4}User Page}{14}{subsection.4.4}%
\contentsline {subsection}{\numberline {4.5}Apartment Listings}{15}{subsection.4.5}%
\contentsline {subsection}{\numberline {4.6}Search}{16}{subsection.4.6}%
\contentsline {subsection}{\numberline {4.7}Recommendations}{17}{subsection.4.7}%
\contentsline {subsection}{\numberline {4.8}Billing}{17}{subsection.4.8}%
\contentsline {subsubsection}{\numberline {4.8.1}Bill Details}{18}{subsubsection.4.8.1}%
\contentsline {subsection}{\numberline {4.9}View Account}{18}{subsection.4.9}%
\contentsline {subsubsection}{\numberline {4.9.1}Rental Information}{19}{subsubsection.4.9.1}%
\contentsline {subsection}{\numberline {4.10}Admin}{19}{subsection.4.10}%
\contentsline {subsubsection}{\numberline {4.10.1}Create Complex}{20}{subsubsection.4.10.1}%
\contentsline {subsubsection}{\numberline {4.10.2}Create Apartment}{21}{subsubsection.4.10.2}%
\contentsline {subsubsection}{\numberline {4.10.3}Check User History}{22}{subsubsection.4.10.3}%
\contentsline {subsubsection}{\numberline {4.10.4}Process Rental Requests}{22}{subsubsection.4.10.4}%
\contentsline {subsubsection}{\numberline {4.10.5}Late Fees}{22}{subsubsection.4.10.5}%
\contentsline {subsubsection}{\numberline {4.10.6}Process Cancellation Requests}{22}{subsubsection.4.10.6}%
\contentsline {subsubsection}{\numberline {4.10.7}Generate Reports}{23}{subsubsection.4.10.7}%
