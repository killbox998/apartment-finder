CREATE TABLE AMENITIES(
  AmenitiesID int NOT NULL AUTO_INCREMENT,
  Laundry varchar(150) DEFAULT NULL,
  Parking varchar(150) DEFAULT NULL,
  Flooring varchar(150) DEFAULT NULL,
  Dishwasher varchar(150) DEFAULT NULL,
  AirConditioner varchar(150) DEFAULT NULL,
  Patio varchar(150) DEFAULT NULL,
  Gym varchar(150) DEFAULT NULL,
  Pool varchar(150) DEFAULT NULL,
  PRIMARY KEY (AmenitiesID)
) 

CREATE TABLE APARTMENT (
  ComplexID int NOT NULL,
  ApartmentID int NOT NULL AUTO_INCREMENT,
  Description varchar(500) DEFAULT NULL,
  ApartmentNumber int NOT NULL,
  Deposit decimal(13,2) NOT NULL,
  MonthRent decimal(13,2) NOT NULL,
  SquareFeet int NOT NULL,
  Bedrooms int NOT NULL,
  Bathrooms int NOT NULL,
  PRIMARY KEY (ApartmentID),
  KEY ComplexID (ComplexID),
 FOREIGN KEY (ComplexID) REFERENCES COMPLEX (ComplexID) ON DELETE CASCADE
) 

CREATE TABLE BILL (
  BillID int NOT NULL AUTO_INCREMENT,
  RentalID int NOT NULL,
  AmountDue decimal(13,2) NOT NULL,
  BillCreated date NOT NULL,
  BillDue date NOT NULL,
  LateFeeAssessed tinyint NOT NULL DEFAULT '0',
  PRIMARY KEY (BillID),
  KEY RentalID (RentalID),
  FOREIGN KEY (RentalID) REFERENCES RENTAL (RentalID)
) 

CREATE TABLE COMPLEX (
  ComplexID int NOT NULL AUTO_INCREMENT,
  State char(2) NOT NULL,
  City varchar(50) NOT NULL,
  StreetName varchar(50) NOT NULL,
  StreetNumber int NOT NULL,
  isCloseToCampus tinyint(1) NOT NULL,
  petsAllowed tinyint(1) NOT NULL,
  AmenitiesID int DEFAULT NULL,
  IsHouse tinyint NOT NULL,
  PRIMARY KEY (ComplexID),
  FOREIGN KEY (AmenitiesID) REFERENCES AMENITIES (AmenitiesID)
) 

CREATE TABLE PAGEHITS (
  UserID int NOT NULL,
  ApartmentID int NOT NULL,
  Hits int NOT NULL,
  PRIMARY KEY (UserID,ApartmentID),
  FOREIGN KEY (UserID) REFERENCES USER (UserID),
  FOREIGN KEY (ApartmentID) REFERENCES APARTMENT (ApartmentID) ON DELETE CASCADE
) 

CREATE TABLE PAGEPERMS (
  Name varchar(50) NOT NULL,
  MustBeUser tinyint NOT NULL,
  MustBeAdmin tinyint NOT NULL,
  PRIMARY KEY (Name)
) 

CREATE TABLE PAYMENT (
  PaymentID int NOT NULL AUTO_INCREMENT,
  UserID int NOT NULL,
  BillID int NOT NULL,
  Amount decimal(9,2) NOT NULL,
  PaymentDate date NOT NULL,
  PRIMARY KEY (PaymentID),
  KEY UserID (UserID),
  KEY BillID (BillID),
  FOREIGN KEY (UserID) REFERENCES USER (UserID),
  FOREIGN KEY (BillID) REFERENCES BILL (BillID)
) 

CREATE TABLE RENTAL (
  RentalID int NOT NULL AUTO_INCREMENT,
  ApartmentID int DEFAULT NULL,
  PurchaseDate date DEFAULT NULL,
  StartDate date DEFAULT NULL,
  EndDate date DEFAULT NULL,
  LeaseRefundDate date DEFAULT NULL,
  LateFee decimal(5,2) NOT NULL,
  Status tinyint DEFAULT NULL,
  StatusReason varchar(100) DEFAULT NULL,
  CancelDate date DEFAULT NULL,
  CancelReason varchar(2000) DEFAULT NULL,
  RequestDecision tinyint(1) DEFAULT NULL,
  AdminResponse varchar(100) DEFAULT NULL,
  PRIMARY KEY (RentalID),
  FOREIGN KEY (ApartmentID) REFERENCES APARTMENT (ApartmentID) ON DELETE CASCADE
) 

CREATE TABLE RENTAL_USERS (
  RentalID int NOT NULL,
  UserID int NOT NULL,
  PrimaryRenter tinyint NOT NULL DEFAULT '0',
  PRIMARY KEY (RentalID,UserID),
  KEY UserID (UserID),
  FOREIGN KEY (RentalID) REFERENCES RENTAL (RentalID),
  FOREIGN KEY (UserID) REFERENCES USER (UserID)
) 

CREATE TABLE REVIEW (
  UserID int NOT NULL,
  RentalID int NOT NULL,
  Rating int NOT NULL,
  Review varchar(2000) DEFAULT NULL,
  PRIMARY KEY (UserID,RentalID),
  FOREIGN KEY (UserID) REFERENCES USER (UserID),
  FOREIGN KEY (RentalID) REFERENCES RENTAL (RentalID)
) 




CREATE TABLE USER (
  UserID int NOT NULL AUTO_INCREMENT,
  LastName varchar(50) NOT NULL,
  FirstName varchar(50) NOT NULL,
  Email varchar(150) NOT NULL,
  PasswordHash varchar(64) NOT NULL,
  PasswordSalt varchar(50) NOT NULL,
  CosignedBy int DEFAULT NULL,
  IsAdmin tinyint NOT NULL DEFAULT '0',
  PRIMARY KEY (UserID),
  FOREIGN KEY (CosignedBy) REFERENCES USER (UserID)
) 


CREATE 
VIEW AVAILABLE_APARTMENTS AS
    SELECT 
        APARTMENT.ApartmentID
    FROM
        APARTMENT
    WHERE
        EXISTS( SELECT 
                RENTAL.RentalID
            FROM
                RENTAL
            WHERE
                ((RENTAL.ApartmentID = APARTMENT.ApartmentID)
                    AND (CURDATE() BETWEEN RENTAL.StartDate AND RENTAL.EndDate)))
            IS FALSE;


CREATE 
VIEW BILL_DETAILS AS
    SELECT 
        BILL.BillID,
        (BILL.AmountDue - COALESCE((SELECT 
                        SUM(PAYMENT.Amount)
                    FROM
                        PAYMENT
                    WHERE
                        (PAYMENT.BillID = BILL.BillID)),
                0)) AS AmountLeft,
        ((BILL.BillDue < (SELECT 
                PAYMENT.PaymentDate
            FROM
                PAYMENT
            WHERE
                (PAYMENT.BillID = BILL.BillID)
            ORDER BY PAYMENT.PaymentDate DESC
            LIMIT 1))
            OR ((BILL.BillDue BETWEEN BILL.BillCreated AND CURDATE())
            AND ((BILL.AmountDue - COALESCE((SELECT 
                        SUM(PAYMENT.Amount)
                    FROM
                        PAYMENT
                    WHERE
                        (PAYMENT.BillID = BILL.BillID)),
                0)) > 0))) AS IsLate
    FROM
        BILL
    GROUP BY BILL.BillID;
    
    
CREATE 
VIEW RENTAL_LENGTH AS
    SELECT 
        RENTAL.RentalID AS RentalID,
        TIMESTAMPDIFF(MONTH,
            RENTAL.StartDate,
            RENTAL.EndDate) AS Length
    FROM
        RENTAL;




