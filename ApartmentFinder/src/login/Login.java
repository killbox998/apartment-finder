package login;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Base64;

import database.Database;

public class Login {

	public static LoginToken login(String email, String password){
		ResultSet emailCorrect = null;
		
	
		PasswordSecurity ps = new PasswordSecurity(password);
		try {			
			emailCorrect = Database.query("SELECT UserID, PasswordHash, PasswordSalt FROM USER WHERE Email = \'" + email + "\';");
			if(emailCorrect.next()){
				int userId = emailCorrect.getInt("UserID");
				String salt = emailCorrect.getString("PasswordSalt");
				byte[] passwordSalt = Base64.getDecoder().decode(salt);
				password = ps.hashPassword(passwordSalt);
				
				if(password.equals(emailCorrect.getString("PasswordHash"))) {
				return new LoginToken(userId, true, true);
				}
				else {
					return new LoginToken(userId, true, false);
				}
			}
			else{
				return new LoginToken(-1, false, false);
			}
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}	
		return new LoginToken(-1, false, false);
	}
		
	
}
