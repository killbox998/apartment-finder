package search;


import java.sql.ResultSet;
import java.sql.SQLException;

import database.Database;

public class Search {

	

	
	
	public static ResultSet search(SearchQuery sq) throws SQLException
	{
	
				
				StringBuilder sb = new StringBuilder();
				sb.append("SELECT APARTMENT.ApartmentID FROM AVAILABLE_APARTMENTS INNER JOIN APARTMENT ON APARTMENT.ApartmentID = AVAILABLE_APARTMENTS.ApartmentID INNER JOIN COMPLEX ON APARTMENT.ComplexID = COMPLEX.ComplexID INNER JOIN AMENITIES ON COMPLEX.AmenitiesID = AMENITIES.AmenitiesID WHERE");
				
				if(sq.maxRent != -1) {sb.append(" APARTMENT.MonthRent <= " + sq.maxRent + " AND ");}
				if(sq.minRent != -1) {sb.append(" APARTMENT.MonthRent >= " + sq.minRent + " AND ");}
				if(sq.minSqrFeet != -1) {sb.append(" APARTMENT.SquareFeet >= " + sq.minSqrFeet + " AND ");}
				if(sq.maxSqrFeet != -1) {sb.append(" APARTMENT.SquareFeet <= " + sq.maxSqrFeet + " AND ");}
				if(sq.bedrooms != -1) {sb.append(" APARTMENT.Bedrooms = " + sq.bedrooms + " AND ");}
				if(sq.bathrooms != -1) {sb.append(" APARTMENT.Bathrooms = " + sq.bathrooms + " AND ");}
				if(sq.state != null) {sb.append(" APARTMENT.State = " + sq.state + " AND ");}
				if(sq.city != null) {sb.append(" APARTMENT.City = " + sq.city + " AND ");}
				
				if(sq.closeToCampus != -1) {sb.append(" COMPLEX.isCloseToCampus = " + sq.closeToCampus + " AND ");}
				if(sq.petsAllowed != -1) {sb.append(" COMPLEX.petsAllowed = " + sq.petsAllowed + " AND ");}
				if(sq.isHouse != -1) {sb.append(" COMPLEX.IsHouse = " + sq.isHouse + " AND ");}
				
				if(sq.laundryType != null) {sb.append(" AMENITIES.Laundry = " +  sq.laundryType + " AND ");}
				if(sq.parkingType != null) {sb.append(" AMENITIES.Parking = " +  sq.parkingType + " AND ");}
				if(sq.flooringType != null) {sb.append(" AMENITIES.Flooring = " +  sq.flooringType + " AND ");}
				if(sq.hasDishwasher != -1) {sb.append(" AMENITIES.Dishwasher = \'yes\' AND ");}
				if(sq.hasAirConditioner != -1) {sb.append(" AMENITIES.AirConditioner = \'yes\' AND ");}
				if(sq.hasPatio != -1) {sb.append(" AMENITIES.Patio = \'yes\' AND ");}
				if(sq.hasGym != -1) {sb.append(" AMENITIES.Gym = \'yes\' AND ");}
				if(sq.hasPool != -1) {sb.append(" AMENITIES.Pool = \'yes\' AND ");}

				
				sb.append("TRUE;");
				//System.out.println(sb.toString());
				return Database.query(sb.toString());


		
	}

}
