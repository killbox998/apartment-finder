package search;

public class SearchQuery {
	public int bedrooms = -1;//dont search if -1
	public  int bathrooms = -1;
	public String state = null;
	public  String city = null;
//	int minPrice = -1;
	public  int maxRent = -1;
	public int minRent = -1;
	public  int minSqrFeet = -1;
	public int maxSqrFeet = -1;
	public int closeToCampus = -1;
	public int petsAllowed = -1;
	public int isHouse = -1;
	public String laundryType = null;
	public String parkingType = null;
	public String flooringType = null;
	public int hasDishwasher = -1;
	public int hasAirConditioner = -1;
	public int hasPatio = -1;
	public int hasGym = -1;
	public int hasPool = -1;
	public int hasGarbageDisposal = -1;

}
