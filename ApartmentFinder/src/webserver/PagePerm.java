package webserver;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import database.Database;

public class PagePerm {

	private static List<PagePerm> perms = new ArrayList<PagePerm>();

	String name;
	boolean mustBeUser;
	boolean mustBeAdmin;

	public static void init() {
		try {
			ResultSet rs = Database.query("SELECT * FROM PAGEPERMS;");
			while (rs.next()) {
				PagePerm temp = new PagePerm(rs.getString("Name"), rs.getBoolean("MustBeUser"),
						rs.getBoolean("MustBeAdmin"));
				perms.add(temp);
			}
		} catch (SQLException e1) {

			e1.printStackTrace();
			System.exit(-1);
		}
	}

	public static PagePerm findPage(String page) {
		for (PagePerm p : perms) {
			if (p.name.equals(page)) {
				return p;
			}
		}
		return null;

	}

	private PagePerm(String name, boolean mustBeUser, boolean mustBeAdmin) {
		this.name = name;
		this.mustBeUser = mustBeUser;
		this.mustBeAdmin = mustBeAdmin;
	}

}
