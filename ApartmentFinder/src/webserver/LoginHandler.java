package webserver;

import java.util.Map;

import com.sun.net.httpserver.HttpExchange;

import login.Login;
import login.LoginToken;

public class LoginHandler extends RequestHandler {

	public LoginHandler() {
		super("/login", "POST");
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean handle(HttpExchange he, String params) {
		Map<String, String> values = parsePostBody(he.getRequestBody());
		
		if(values.containsKey("logout"))
		{
			//System.out.println("ooofff");
			UserCookie.delete(he.getRequestHeaders().getFirst("Cookie"));
			redirect(he, "/");
			return true;
		}
		
		LoginToken lt = Login.login(values.get("email"), values.get("password"));
		String redirect = null;
		// successful login
		if (lt.accountExists && lt.loginCorrect) {
			he.getResponseHeaders().add("Set-Cookie", new UserCookie(lt.userID).getCookie());
			redirect = "/user.htm";
		}
		// failed login
		else {
			if (lt.accountExists)
				redirect = "/login.html?message=badlogin";
			else
				redirect = "/login.html?message=nouser";
		}
		redirect(he, redirect);
		return true;
	}

}
