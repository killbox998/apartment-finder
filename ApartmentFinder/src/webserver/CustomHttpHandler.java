package webserver;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import database.Database;
import login.CreateAccount;
import login.Login;
import login.LoginToken;

public class CustomHttpHandler implements HttpHandler {

	@Override
	public void handle(HttpExchange he) throws IOException {
		System.out.println("Request");
		String uri = he.getRequestURI().toString();
		System.out.println(uri);
		String parameters = null;
		if (uri.indexOf('?') != -1) {
			parameters = uri.substring(uri.indexOf('?') + 1, uri.length());
			uri = uri.substring(0, uri.indexOf('?'));
		}
		// System.out.println("Cookie :\"" + he.getRequestHeaders().getFirst("Cookie") +
		// "\"");
		// Determine the type of HTTP request and directs to the correct handler

		PagePerm p = PagePerm.findPage(uri);
		if (p != null) {
			UserCookie uc = UserCookie.getCookie(he.getRequestHeaders().getFirst("Cookie"));
			if (uc == null && p.mustBeUser) {
				// forbidden must be user
				RequestHandler.loadErrorPage(403, he);
				return;
			} else if (p.mustBeAdmin && !uc.isAdmin()) {
				// forbidden must be admin
				RequestHandler.loadErrorPage(403, he);
				return;
			}
		}
		try {
		if ("GET".equals(he.getRequestMethod())) {
			if (!RequestHandler.handleGetRequest(he, uri, parameters)) {
				loadStaticFile(he);
				return;
			}
		} else if ("POST".equals(he.getRequestMethod())) {
			if (!RequestHandler.handlePostRequest(he, uri, parameters)) {
				System.out.println("no POST handler found");
				RequestHandler.loadErrorPage(404, he);
				return;
			}
		} 
		}catch(Exception e)
		{
			e.printStackTrace();
			RequestHandler.loadErrorPage(500, he);
			return;
		}
	}

	// loads a file from the html directory. If the file doesn't exist it returns an
	// error page to the user.
	private void loadStaticFile(HttpExchange he) throws IOException {
		OutputStream outputStream = he.getResponseBody();
		StringBuilder htmlBuilder = new StringBuilder();
		InputStream f;

		String uri = he.getRequestURI().toString();
		String parameters = null;
		if (uri.indexOf('?') != -1) {
			parameters = uri.substring(uri.indexOf('?') + 1, uri.length());
			uri = uri.substring(0, uri.indexOf('?'));
		}
		if (uri.equals("/")) {
			f = this.getClass().getResourceAsStream("/html/index.html");

		} else {
			f = this.getClass().getResourceAsStream("/html" + uri);

		}
		try {
			if(f == null)
			{
				RequestHandler.loadErrorPage(404, he);
				return;
			}
			he.sendResponseHeaders(200, htmlBuilder.toString().length());

			//outputStream.write(new BufferedReader(new InputStreamReader(f)).lines().collect(Collectors.joining("\n")).getBytes());

			while(f.available() > 0) {
			long fileSize = f.available();
			 
            byte[] buf = new byte[(int) fileSize];
 
			f.read(buf);
 
            outputStream.write(buf);
			}
			
			outputStream.flush();

			outputStream.close();

			he.close();

		} catch (Exception e) {
			e.printStackTrace();
			RequestHandler.loadErrorPage(404, he);
		}
	}

}
// returns the appropriate error page for each code.

// parse the query string into a map of keys
