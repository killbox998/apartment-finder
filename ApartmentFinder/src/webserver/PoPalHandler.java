package webserver;

import java.sql.SQLException;
import java.util.Map;

import com.sun.net.httpserver.HttpExchange;

import database.Database;

public class PoPalHandler extends RequestHandler{

	public PoPalHandler() {
		super("/makepayment", "POST");
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean handle(HttpExchange he, String params) {
		UserCookie uc = UserCookie.getCookie(he.getRequestHeaders().getFirst("Cookie"));
		Map<String, String> values = parsePostBody(he.getRequestBody());
		try {
			int billid = Integer.parseInt(values.get("billid"));
			if(canUserAccessResource(he, Database.query("SELECT UserID FROM BILL INNER JOIN RENTAL ON BILL.RentalID = RENTAL.RentalID INNER JOIN RENTAL_USERS ON RENTAL.RentalID = RENTAL_USERS.RentalID WHERE BillID = "+billid+";")))
			{
				StringBuilder sb = new StringBuilder();
				sb.append("INSERT INTO PAYMENT(UserID, BillID, Amount, PaymentDate) VALUES(");
				sb.append(uc.getUserID());
				sb.append(", ");
				sb.append(billid);
				sb.append(", ");
				sb.append(values.get("amount"));
				sb.append(", ");
				sb.append("CURDATE()");
				sb.append(");");
				Database.execute(sb.toString());
				redirect(he, "/billdata.html?id=" + billid);
				return true;
			}
			else
			{
				loadErrorPage(403, he);
				return false;
			}
		} catch (SQLException e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
			loadErrorPage(500, he);
			return false;
		}
		
		
		
		
	}

}
