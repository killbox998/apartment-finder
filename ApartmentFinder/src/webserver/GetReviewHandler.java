package webserver;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.sun.net.httpserver.HttpExchange;

import database.Database;

public class GetReviewHandler extends RequestHandler {

	public GetReviewHandler() {
		super("/getreviews", "GET");
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean handle(HttpExchange he, String params){
		
		Map<String, String> values = parseGetParams(params);
		try {
		int id = Integer.parseInt(values.get("id"));
		
			ResultSet rs = Database.query("SELECT USER.UserID, USER.FirstName, USER.LastName, Rating, Review FROM REVIEW INNER JOIN USER ON REVIEW.UserID = USER.USERID INNER JOIN RENTAL on RENTAL.RentalID = REVIEW.RentalID WHERE ApartmentID = "+id+";");
			JSONArray a = new JSONArray();
			while(rs.next())
			{
				JSONObject o = new JSONObject();
				o.put("firstname", rs.getString("FirstName"));
				o.put("lastname", rs.getString("LastName"));
				o.put("userid", rs.getString("UserID"));
				o.put("rating", rs.getInt("Rating"));
				o.put("review", rs.getString("Review"));
				a.put(o);
				
			}
			OutputStream outputStream = he.getResponseBody();
			he.getResponseHeaders().add("content-type", "application/json");
			he.sendResponseHeaders(200, 0);
			outputStream.write(a.toString().getBytes());
			outputStream.flush();
			outputStream.close();
			he.close();
			return true;
			
		
		} catch (SQLException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			loadErrorPage(500, he);
			return false;
		}
		
	}

}
