package webserver;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import com.sun.net.httpserver.HttpExchange;

import User.CancelRental;
import database.Database;

public class CancelRequestHandler extends RequestHandler {

	public CancelRequestHandler() {
		super("/cancelrequest", "POST");
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean handle(HttpExchange he, String params) {
		Map<String, String> values = parsePostBody(he.getRequestBody());
		try {
		Date d = new SimpleDateFormat("yyyy-MM-dd").parse(values.get("enddate"));
		
			new CancelRental(values.get("enddate"), Database.escapeString(values.get("reason")), Integer.parseInt(values.get("rental")));
			redirect(he, "/viewrental.html?id=" + Integer.parseInt(values.get("rental")));
			return true;
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			loadErrorPage(500, he);
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			loadErrorPage(500, he);
			return false;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			loadErrorPage(500, he);
			return false;
		}
	}

}
