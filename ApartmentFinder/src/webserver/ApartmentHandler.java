package webserver;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.sun.net.httpserver.HttpExchange;

import database.Database;

public class ApartmentHandler extends RequestHandler {

	public ApartmentHandler() {
		super("/apartment", "GET");
	}

	@Override
	public boolean handle(HttpExchange he, String params) {

		try {
			Map<String, String> values = parseGetParams(params);
			String s = values.get("id");
			if (s.equals("list")) {
				JSONArray a = new JSONArray();

				ResultSet rs = Database.query(
						"SELECT * FROM APARTMENT INNER JOIN COMPLEX ON APARTMENT.ComplexID=COMPLEX.ComplexID WHERE EXISTS (SELECT * FROM AVAILABLE_APARTMENTS WHERE AVAILABLE_APARTMENTS.ApartmentID = APARTMENT.ApartmentID) ORDER BY MonthRent;");
				while (rs.next()) {

					JSONObject o = new JSONObject();
					o.put("id", rs.getInt("ApartmentID"));
					o.put("complexid", rs.getInt("ComplexID"));
					o.put("description", rs.getString("Description"));
					o.put("apartmentnumber", rs.getInt("ApartmentNumber"));
					o.put("deposit", rs.getDouble("Deposit"));
					o.put("monthrent", rs.getDouble("MonthRent"));
					o.put("squarefeet", rs.getInt("SquareFeet"));
					o.put("bedrooms", rs.getInt("Bedrooms"));
					o.put("bathrooms", rs.getInt("Bathrooms"));
					StringBuilder sb = new StringBuilder();
					sb.append(rs.getInt("StreetNumber"));
					sb.append(" ");
					sb.append(rs.getString("StreetName"));
					if(!rs.getBoolean("IsHouse")) {
					sb.append(" Apartment ");
					sb.append(rs.getInt("ApartmentNumber"));
					}
					sb.append(", ");
					sb.append(rs.getString("City"));
					sb.append(", ");
					sb.append(rs.getString("State"));
					o.put("address", sb.toString());

					a.put(o);
				}

				OutputStream outputStream = he.getResponseBody();
				he.getResponseHeaders().add("content-type", "application/json");
				he.sendResponseHeaders(200, 0);
				outputStream.write(a.toString().getBytes());
				outputStream.flush();
				outputStream.close();
				he.close();
				return true;
			} else {
				//System.out.println(Integer.parseInt(values.get("id")));
				int id = Integer.parseInt(values.get("id"));
				ResultSet rs = Database.query(
						"SELECT * FROM APARTMENT INNER JOIN COMPLEX ON APARTMENT.ComplexID=COMPLEX.ComplexID INNER JOIN AMENITIES ON COMPLEX.AmenitiesID=AMENITIES.AmenitiesID WHERE ApartmentID="
								+ id + ";");
				if (!rs.next()) {
					loadErrorPage(404, he);
					return false;
				}
				JSONObject o = new JSONObject();
				o.put("id", id);
				o.put("complexid", rs.getInt("ComplexID"));
				o.put("description", rs.getString("Description"));
				o.put("apartmentnumber", rs.getInt("ApartmentNumber"));
				o.put("deposit", rs.getDouble("Deposit"));
				o.put("monthrent", rs.getDouble("MonthRent"));
				o.put("squarefeet", rs.getInt("SquareFeet"));
				o.put("bedrooms", rs.getInt("Bedrooms"));
				o.put("bathrooms", rs.getInt("Bathrooms"));
				o.put("ishouse", rs.getBoolean("IsHouse"));
				o.put("petsallowed", rs.getBoolean("petsAllowed")? "Yes" : "No");
				StringBuilder sb = new StringBuilder();
				sb.append(rs.getInt("StreetNumber"));
				sb.append(" ");
				sb.append(rs.getString("StreetName"));
				sb.append(" ");
				sb.append(rs.getString("City"));
				sb.append(", ");
				sb.append(rs.getString("State"));
				o.put("address", sb.toString());
				o.put("laundry", rs.getString("Laundry") != null ? rs.getString("Laundry") : "No");
				o.put("parking", rs.getString("Parking") != null ? rs.getString("Parking") : "No");
				o.put("flooring", rs.getString("Flooring") != null ? rs.getString("Flooring") : "No");
				o.put("dishwasher", rs.getString("Dishwasher") != null ? rs.getString("Dishwasher") : "No");
				o.put("airconditioner", rs.getString("AirConditioner") != null ? rs.getString("AirConditioner") : "No");
				o.put("patio", rs.getString("Patio") != null ? rs.getString("Patio") : "No");
				o.put("gym", rs.getString("Gym") != null ? rs.getString("Gym") : "No");
				o.put("pool", rs.getString("Pool") != null ? rs.getString("Pool") : "No");
				OutputStream outputStream = he.getResponseBody();
				he.getResponseHeaders().add("content-type", "application/json");
				he.sendResponseHeaders(200, 0);
				outputStream.write(o.toString().getBytes());
				outputStream.flush();
				outputStream.close();
				he.close();
				return true;
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
			loadErrorPage(400, he);
			return false;
		} catch (IOException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			loadErrorPage(500, he);
			return false;
		}
	}

}
