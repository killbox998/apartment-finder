package webserver;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONObject;

import com.sun.net.httpserver.HttpExchange;

import database.Database;

public class AdminRentalRequestHandler extends RequestHandler {

	public AdminRentalRequestHandler() {
		super("/getrentalrequests", "GET");
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean handle(HttpExchange he, String params) {
		JSONArray a = new JSONArray();
		try {
			ResultSet rs = Database.query("SELECT * FROM RENTAL WHERE Status IS NULL;");
			while(rs.next())
			{
				JSONObject o = new JSONObject();
				o.put("id", rs.getInt("RentalID"));
				o.put("startdate", rs.getDate("StartDate"));
				o.put("enddate", rs.getDate("EndDate"));
				JSONArray users = new JSONArray();
				ResultSet userSet = Database.query("SELECT USER.UserID, FirstName, LastName, Email, PrimaryRenter FROM RENTAL_USERS INNER JOIN USER ON RENTAL_USERS.UserID=USER.UserID WHERE RentalID="+rs.getInt("RentalID")+";");
				while(userSet.next())
				{
					JSONObject userObject = new JSONObject();
					userObject.put("id", userSet.getInt("UserID"));
					userObject.put("firstname", userSet.getString("FirstName"));
					userObject.put("lastname", userSet.getString("LastName"));
					userObject.put("email", userSet.getString("Email"));
					userObject.put("primary", userSet.getBoolean("PrimaryRenter"));
					users.put(userObject);
				}
				o.put("renters", users);
				a.put(o);
			}
			
			OutputStream outputStream = he.getResponseBody();
			he.getResponseHeaders().add("content-type", "application/json");
			he.sendResponseHeaders(200, 0);
			outputStream.write(a.toString().getBytes());
			outputStream.flush();
			outputStream.close();
			
		} catch (SQLException e) {
			loadErrorPage(500, he);
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			loadErrorPage(500, he);

			e.printStackTrace();
			return false;
		}
		return false;
	}

}
