package webserver;

import java.sql.SQLException;
import java.util.Map;

import com.sun.net.httpserver.HttpExchange;

import database.Database;

public class CreateApartmentHandler extends RequestHandler {

	public CreateApartmentHandler() {
		super("/createapartment", "POST");
		
	}

	@Override
	public boolean handle(HttpExchange he, String params) {
		try {
		Map<String, String> values = parsePostBody(he.getRequestBody());
		int complexID = Integer.parseInt(values.get("complex"));
		int apartmentNumber = Integer.parseInt(values.get("apartmentnumber"));
		double monthRent = Double.parseDouble(values.get("monthrent"));
		double deposit = Double.parseDouble(values.get("deposit"));
		int squareFeet = Integer.parseInt(values.get("squarefeet"));
		int bathrooms = Integer.parseInt(values.get("bedrooms"));
		int bedrooms = Integer.parseInt(values.get("bathrooms"));
		String description = values.get("description");
		//System.out.println("INSERT INTO APARTMENT(ComplexID, ApartmentNumber, MonthRent, Deposit, SquareFeet, Bedrooms, Bathrooms, Description)" +
		//"VALUES ("+complexID+", "+apartmentNumber+", "+monthRent+", "+deposit+", "+squareFeet+", "+bedrooms+", " +bathrooms +", \'"+description+"\');");	
		Database.execute("INSERT INTO APARTMENT(ComplexID, ApartmentNumber, MonthRent, Deposit, SquareFeet, Bedrooms, Bathrooms, Description)" +
		"VALUES ("+complexID+", "+apartmentNumber+", "+monthRent+", "+deposit+", "+squareFeet+", "+bedrooms+", " +bathrooms +", \'"+description+"\');");
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			redirect(he, "/createapartment.html?message=error");
			return false;
		}
		redirect(he, "/admin.htm");
		return true;
		
	}

}
