package webserver;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.sun.net.httpserver.HttpExchange;

import database.Database;

public class UserContractInfoHandler extends RequestHandler {

	public UserContractInfoHandler() {
		super("/getusercontractinfo", "GET");
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean handle(HttpExchange he, String params) {
	
		Map<String, String> values = parseGetParams(params);
		JSONObject o = new JSONObject();
		try {
		int id = Integer.parseInt(values.get("id"));
		
		
			ResultSet rs = Database.query("SELECT USER.UserID, USER.FirstName, USER.LastName, USER.Email, C.Email AS CosignerEmail FROM USER LEFT JOIN USER AS C ON USER.CosignedBy=C.UserID WHERE USER.UserID="+id+";");
			if(!rs.next())
			{
				loadErrorPage(404, he);
				return false;
			}
			o.put("id",id);
			if(rs.getString("CosignerEmail") != null)
			o.put("cosigner", rs.getString("CosignerEmail"));
			else
				o.put("cosigner", "none");
			o.put("firstname", rs.getString("FirstName"));
			o.put("lastname", rs.getString("LastName"));
			o.put("email", rs.getString("Email"));
			JSONArray a = new JSONArray();
			ResultSet late = Database.query("SELECT USER.UserID, COUNT(IsLate) AS TotalBills, SUM(IsLate) AS LateBills FROM BILL_DETAILS INNER JOIN BILL ON BILL_DETAILS.BillID = BILL.BillID INNER JOIN RENTAL ON BILL.RentalID = RENTAL.RentalID INNER JOIN RENTAL_USERS ON RENTAL.RentalID = RENTAL_USERS.RentalID INNER JOIN USER ON RENTAL_USERS.UserID = USER.UserID WHERE USER.UserID = "+id+" AND IsLate IS NOT NULL GROUP BY USER.UserID;");
			if(late.next()) {
			o.put("totalbills", late.getInt("TotalBills"));
			o.put("latebills", late.getInt("LateBills"));
			}else
			{
				o.put("totalbills", 0);
				o.put("latebills", 0);
			}
			OutputStream outputStream = he.getResponseBody();
			he.getResponseHeaders().add("content-type", "application/json");
			he.sendResponseHeaders(200, 0);
			outputStream.write(o.toString().getBytes());
			outputStream.flush();
			outputStream.close();
			he.close();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			loadErrorPage(500, he);
			return false;
		
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			loadErrorPage(500, he);
			e.printStackTrace();
			return false;
		}
	
		
		
		}

}
