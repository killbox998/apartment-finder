package webserver;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.sun.net.httpserver.HttpExchange;

import database.Database;

public class GetBillsHandler extends RequestHandler {

	public GetBillsHandler() {
		super("/getbills", "GET");
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean handle(HttpExchange he, String params) {
		try {
		Map<String, String> values = parseGetParams(params);
		UserCookie uc = UserCookie.getCookie(he.getRequestHeaders().getFirst("Cookie"));
		String s = values.get("id");
		if(s.equals("list")) {
			JSONObject bills = new JSONObject();
			ResultSet rs = Database.query("SELECT  BILL.BillID, BillDue, AmountDue, AmountLeft, IsLate  FROM BILL INNER JOIN BILL_DETAILS ON BILL.BillID = BILL_DETAILS.BillID INNER JOIN RENTAL ON BILL.RentalID = RENTAL.RentalID INNER JOIN RENTAL_USERS ON RENTAL.RentalID = RENTAL_USERS.RentalID INNER JOIN USER ON RENTAL_USERS.UserID = USER.UserID WHERE USER.UserID="+uc.getUserID()+" AND CURDATE() BETWEEN BILL.BillCreated AND BILL.BillDue ORDER BY BillDue desc;");
			JSONArray active = new JSONArray();
			while(rs.next())
			{
				JSONObject o = new JSONObject();
				o.put("id", rs.getInt("BillID"));
				o.put("duedate", new SimpleDateFormat("yyyy-MM-dd").format( rs.getDate("BillDue")));
				o.put("amountdue", rs.getDouble("AmountDue"));
				o.put("amountleft", rs.getDouble("AmountLeft"));
				o.put("islate", rs.getBoolean("IsLate"));
				active.put(o);
			}
			bills.put("current", active);
		 rs = Database.query("SELECT  BILL.BillID, BillDue, AmountDue, AmountLeft, IsLate  FROM BILL INNER JOIN BILL_DETAILS ON BILL.BillID = BILL_DETAILS.BillID INNER JOIN RENTAL ON BILL.RentalID = RENTAL.RentalID INNER JOIN RENTAL_USERS ON RENTAL.RentalID = RENTAL_USERS.RentalID INNER JOIN USER ON RENTAL_USERS.UserID = USER.UserID WHERE USER.UserID="+uc.getUserID()+" AND BillDue BETWEEN BILL.BillCreated AND CURDATE() ORDER BY BillDue desc;");
			JSONArray past = new JSONArray();
			while(rs.next())
			{
				JSONObject o = new JSONObject();
				o.put("id", rs.getInt("BillID"));
				o.put("duedate", new SimpleDateFormat("yyyy-MM-dd").format( rs.getDate("BillDue")));
				o.put("amountdue", rs.getDouble("AmountDue"));
				o.put("amountleft", rs.getDouble("AmountLeft"));
				o.put("islate", rs.getBoolean("IsLate"));
				past.put(o);
			}
			bills.put("past", past);
			
			
			
			OutputStream outputStream = he.getResponseBody();
			he.sendResponseHeaders(200, 0);
			outputStream.write(bills.toString().getBytes());
			outputStream.flush();
			outputStream.close();
			he.close();
			return true;
		
		}
		else
		{
			int id = Integer.parseInt(s);
			ResultSet rs = Database.query("SELECT  BILL.BillID, BillDue, BillCreated, AmountDue, AmountLeft, IsLate  FROM BILL INNER JOIN BILL_DETAILS ON BILL.BillID = BILL_DETAILS.BillID INNER JOIN RENTAL ON BILL.RentalID = RENTAL.RentalID INNER JOIN RENTAL_USERS ON RENTAL.RentalID = RENTAL_USERS.RentalID INNER JOIN USER ON RENTAL_USERS.UserID = USER.UserID WHERE BILL.BillID = "+id+";");
			if(!rs.next())
			{
				loadErrorPage(404, he);
				return false;
			}
			JSONObject o = new JSONObject();
			o.put("id", rs.getInt("BillID"));
			o.put("due", new SimpleDateFormat("yyyy-MM-dd").format( rs.getDate("BillDue")));
			o.put("created",new SimpleDateFormat("yyyy-MM-dd").format( rs.getDate("BillCreated")));
			o.put("amountdue", rs.getDouble("AmountDue"));
			o.put("amountleft", rs.getDouble("AmountLeft"));
			o.put("islate", rs.getBoolean("IsLate"));
			rs = Database.query("SELECT PaymentID, USER.UserID, Amount, PaymentDate, FirstName, LastName FROM PAYMENT INNER JOIN USER ON PAYMENT.UserID = USER.UserID WHERE PAYMENT.BillID = "+id+";");
			JSONArray a = new JSONArray();
			while(rs.next())
			{
				JSONObject pay = new JSONObject();
				pay.put("id", rs.getInt("PaymentID"));
				pay.put("userid", rs.getInt("UserID"));
				pay.put("amount", rs.getDouble("Amount"));
				pay.put("date", new SimpleDateFormat("yyyy-MM-dd").format( rs.getDate("PaymentDate")));
				pay.put("firstname", rs.getString("FirstName"));
				pay.put("lastname", rs.getString("LastName"));
				a.put(pay);
			}
			o.put("payments", a);
			OutputStream outputStream = he.getResponseBody();
			he.getResponseHeaders().add("content-type", "application/json");
			he.sendResponseHeaders(200, 0);
			outputStream.write(o.toString().getBytes());
			outputStream.flush();
			outputStream.close();
			he.close();
			return true;
		}
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			loadErrorPage(500,he);
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			loadErrorPage(500,he);
			return false;
		}
		
		
		
	}

}
