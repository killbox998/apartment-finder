package webserver;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.StringTokenizer;

import com.sun.net.httpserver.HttpExchange;

import database.Database;

public class RentalRequestHandler extends RequestHandler {

	public RentalRequestHandler() {
		super("/rentalrequest", "POST");
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public boolean handle(HttpExchange he, String params) {
		Map<String, String> values = parsePostBody(he.getRequestBody());
	try {
		int apartmentID = Integer.parseInt(values.get("apartmentid"));
		String startDate = values.get("startdate");
		int length = Integer.parseInt(values.get("length"));
		Calendar sd = Calendar.getInstance();
		
		sd.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(startDate));
		sd.add(Calendar.MONTH, length);
		
		//System.out.println(startDate);
		//System.out.println(new SimpleDateFormat("yyyy-MM-dd").format(sd.getTime()));
		
		UserCookie uc = UserCookie.getCookie(he.getRequestHeaders().getFirst("Cookie"));
		//System.out.println("INSERT INTO RENTAL(ApartmentID, StartDate, EndDate) VALUES("+apartmentID+",\'"+startDate+"\', \'"+new SimpleDateFormat("yyyy-MM-dd").format(sd.getTime())+"');");
		Database.execute("INSERT INTO RENTAL(ApartmentID, PurchaseDate, StartDate, EndDate, LateFee) VALUES("+apartmentID+",CURDATE(),\'"+startDate+"\', \'"+new SimpleDateFormat("yyyy-MM-dd").format(sd.getTime())+"\', 50);");
		ResultSet rs = Database.query("SELECT LAST_INSERT_ID()");
		rs.next();
		int rentalID = rs.getInt(1);
		Database.execute("INSERT INTO RENTAL_USERS(RentalID, UserID, PrimaryRenter) VALUES("+rentalID+", "+uc.getUserID()+", true);");
		if(!values.get("cosigner").equals(""))
		{
			ResultSet user = Database.query("SELECT UserID FROM USER WHERE Email=\'"+Database.escapeString(values.get("cosigner"))+"\';");
			user.next();
			Database.execute("UPDATE USER SET CosignedBy= "+user.getInt("UserID")+" WHERE UserID="+uc.getUserID()+";");
		}
		StringTokenizer st = new StringTokenizer(values.get("roommates"), " ,");
		//System.out.println(values.get("roommates"));
		while(st.hasMoreTokens())
		{
			
			rs = Database.query("SELECT UserID FROM USER WHERE Email=\'"+Database.escapeString(st.nextToken())+"\';");
			rs.next();
		//	System.out.println("INSERT INTO RENTAL_USERS(RentalID, UserID) VALUES("+rentalID+", "+rs.getInt("UserID")+");");
			Database.execute("INSERT INTO RENTAL_USERS(RentalID, UserID) VALUES("+rentalID+", "+rs.getInt("UserID")+");");
			
		}
		redirect(he, "/rentalrequestsuccess.html");
		return true;
	}catch(Exception e)
	{
		e.printStackTrace();
		loadErrorPage(500, he);
		return false;
	}
		
	}

}
