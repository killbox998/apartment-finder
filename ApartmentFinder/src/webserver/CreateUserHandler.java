package webserver;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Map;

import com.sun.net.httpserver.HttpExchange;

import login.CreateAccount;

public class CreateUserHandler extends RequestHandler {

	public CreateUserHandler() {
		super("/createuser", "POST");
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean handle(HttpExchange he, String params) {
		Map<String, String> values = parsePostBody(he.getRequestBody());

		String redirect = null;
		if (!values.get("password").equals(values.get("confirmpassword"))) {
			redirect = "/createuser.html?message=passwordmismatch";
		} else {
			try {
				CreateAccount newUser = new CreateAccount(values.get("email"), values.get("password"),
						values.get("firstname"), values.get("lastname"));
				if (newUser.AccountCreated()) {

					redirect = "/accountcreatesuccess.html";
				} else
					redirect = "/createuser.html?message=alreadyexists";

			} catch (NoSuchAlgorithmException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				redirect = "/createuser.html?message=error";
			}
		}
	redirect(he, redirect);
	return true;
	}

}
