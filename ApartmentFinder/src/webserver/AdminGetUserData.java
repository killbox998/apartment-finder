package webserver;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.sun.net.httpserver.HttpExchange;

import database.Database;

public class AdminGetUserData extends RequestHandler {

	public AdminGetUserData() {
		super("/getuserdata", "GET");
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean handle(HttpExchange he, String params) {

		Map<String, String> values = parseGetParams(params);
		
		try {
			String s = values.get("id");
			if (s.equals("list")) {
				JSONArray a = new JSONArray();

				ResultSet rs = Database.query("SELECT UserID, FirstName, LastName, Email FROM USER ORDER BY LastName;");
				while (rs.next()) {

					JSONObject o = new JSONObject();
					
					o.put("id", rs.getInt("UserID"));
					o.put("firstname", rs.getString("FirstName"));
					o.put("lastname", rs.getString("LastName"));
					o.put("email", rs.getString("Email"));
					a.put(o);
				}

				OutputStream outputStream = he.getResponseBody();
				he.getResponseHeaders().add("content-type", "application/json");
				he.sendResponseHeaders(200, 0);
				outputStream.write(a.toString().getBytes());
				outputStream.flush();
				outputStream.close();
				he.close();
				return true;
			} else {
				int id = Integer.parseInt(values.get("id"));
				JSONObject o = new JSONObject();
				ResultSet rs = Database
						.query("SELECT UserID, FirstName, LastName, Email   FROM USER WHERE UserID=" + id + " ;");
				if (!rs.next()) {
					loadErrorPage(404, he);
					return false;
				}
				o.put("id", id);
				o.put("firstname", rs.getString("FirstName"));
				o.put("lastname", rs.getString("LastName"));
				o.put("email", rs.getString("Email"));
				ResultSet late = Database.query(
						"SELECT USER.UserID, COUNT(IsLate) AS TotalBills, SUM(IsLate) AS LateBills FROM BILL_DETAILS INNER JOIN BILL ON BILL_DETAILS.BillID = BILL.BillID INNER JOIN RENTAL ON BILL.RentalID = RENTAL.RentalID INNER JOIN RENTAL_USERS ON RENTAL.RentalID = RENTAL_USERS.RentalID INNER JOIN USER ON RENTAL_USERS.UserID = USER.UserID WHERE USER.UserID = "
								+ id + " AND IsLate IS NOT NULL GROUP BY USER.UserID;");
				late.next();
				o.put("totalbills", late.getInt("TotalBills"));
				o.put("latebills", late.getInt("LateBills"));
				OutputStream outputStream = he.getResponseBody();
				he.getResponseHeaders().add("content-type", "application/json");
				he.sendResponseHeaders(200, 0);
				outputStream.write(o.toString().getBytes());
				outputStream.flush();
				outputStream.close();
				he.close();
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			loadErrorPage(500, he);
			return false;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			loadErrorPage(500, he);
			e.printStackTrace();
			return false;
		}

	}

}
