package webserver;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import com.sun.net.httpserver.HttpExchange;

import database.Database;

public class CreateReviewHandler extends RequestHandler {

	public CreateReviewHandler() {
		super("/review", "POST");
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean handle(HttpExchange he, String params) {
		UserCookie uc = UserCookie.getCookie(he.getRequestHeaders().getFirst("Cookie"));
		Map<String, String> values = parsePostBody(he.getRequestBody());
		int rating = Integer.parseInt(values.get("rating"));
		int rentalid = Integer.parseInt(values.get("id"));
		
		try {
			Database.execute("INSERT INTO REVIEW(UserID, RentalID, Rating, Review) VALUES ("+uc.getUserID()+", "+rentalid+","+rating+", \'"+Database.escapeString(values.get("review"))+"\');");
			ResultSet rs = Database.query("SELECT ApartmentID FROM RENTAL WHERE RentalID = "+rentalid+";");
			rs.next();
			redirect(he, "/Apartment_view.htm?id=" + rs.getInt("ApartmentID"));
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			loadErrorPage(500, he);
			return false;
		}
		
	}

}
