package webserver;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import com.sun.net.httpserver.HttpExchange;

import database.Database;

public class ApartmentViewHandler extends RequestHandler {

	public ApartmentViewHandler() {
		super("/apartmenthit", "POST");

	}

	@Override
	public boolean handle(HttpExchange he, String params) {
		Map<String, String> values = parsePostBody(he.getRequestBody());
		UserCookie uc = UserCookie.getCookie(he.getRequestHeaders().getFirst("Cookie"));
		try {

			ResultSet rs = Database.query("SELECT * FROM PAGEHITS WHERE UserID=\'" + uc.getUserID()
					+ "\' AND ApartmentID=" + Integer.parseInt(values.get("id")) + "");
			
			if (rs.next()) {

				Database.execute("UPDATE PAGEHITS SET Hits=" + (rs.getInt("Hits") + 1) + " WHERE UserID=\'"
						+ uc.getUserID() + "\' AND ApartmentID="
						+ Integer.parseInt(values.get("id")) + ";");
				he.sendResponseHeaders(200, 0);
				he.close();
				return true;
			} else {
				Database.execute("INSERT INTO PAGEHITS(UserID, ApartmentID, Hits) VALUES(" + uc.getUserID() + ", "
						+ Integer.parseInt(values.get("id")) + ", 1);");
				he.sendResponseHeaders(200, 0);
				he.close();
				return true;
			}
		} catch (NumberFormatException e) {
			loadErrorPage(400, he);
			e.printStackTrace();
		} catch (SQLException e) {
			loadErrorPage(500, he);
			e.printStackTrace();
		} catch (IOException e) {
			loadErrorPage(500, he);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}

}
