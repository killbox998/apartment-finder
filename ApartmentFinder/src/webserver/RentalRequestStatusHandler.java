package webserver;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import com.sun.net.httpserver.HttpExchange;

import database.Database;

public class RentalRequestStatusHandler extends RequestHandler {

	public RentalRequestStatusHandler() {
		super("/updaterequeststatus", "POST");
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean handle(HttpExchange he, String params) {
		Map<String, String> values = parsePostBody(he.getRequestBody());
				try {
				if(values.get("type").equals("accept"))
				{
					//Database.execute("UPDATE RENTAL SET Status=true WHERE RentalID="+Integer.parseInt(values.get("id"))+";");
					
					redirect(he, "/createcontract.html?id=" + values.get("id") + "&userid=" + values.get("userid"));
					return true;
				}else
				{
					Database.execute("UPDATE RENTAL SET Status=false,StatusReason=\'"+Database.escapeString(values.get("reason"))+"\' WHERE RentalID="+Integer.parseInt(values.get("id"))+";");
					redirect(he, "/processrentalrequests.html");
					return true;
				}
				
				
				} catch (NumberFormatException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					loadErrorPage(500, he);
				}
				return false;
	}

}
