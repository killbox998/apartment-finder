package webserver;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import com.sun.net.httpserver.HttpExchange;

import admin.CancellationRequest;
import database.Database;

public class CancelRequestStatusHandler extends RequestHandler {

	public CancelRequestStatusHandler() {
		super("/updatecancelrequeststatus", "POST");
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean handle(HttpExchange he, String params) {
		Map<String, String> values = parsePostBody(he.getRequestBody());
				try {
				if(values.get("type").equals("accept"))
				{
					new CancellationRequest(true, Integer.parseInt(values.get("id")));
					redirect(he, "/processcancelrequests.html");
					return true;
				}else
				{
					new CancellationRequest(false, Integer.parseInt(values.get("id")));
					redirect(he, "/processcancelrequests.html");
					return true;
				}
				
				
				} catch (NumberFormatException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					loadErrorPage(500, he);
				}
				return false;
	}

}
