package recommendations;

import java.sql.ResultSet; 
import java.sql.SQLException;
import database.Database;

public class Recommendations {
	ResultSet rs; 
	private int aptID;
	private double rent;
	private int sqFt;
	private int bathroom;
	private int bedroom;
	
	
	public Recommendations(int userId) throws SQLException{
		rs = Database.query("SELECT ApartmentID FROM PAGEHITS WHERE UserID = " + userId +  " Order By Hits DESC Limit 1;");
		if(rs.next()) {
			this.aptID = rs.getInt("ApartmentID");
			rs = Database.query("SELECT MonthRent, SquareFeet, BedRooms, Bathrooms FROM APARTMENT WHERE ApartmentID = " + aptID);
			rs.next();
			this.rent = rs.getDouble("MonthRent");
			this.sqFt = rs.getInt("SquareFeet");
			this.bedroom = rs.getInt("Bedrooms");
			this.bathroom = rs.getInt("Bathrooms");
		}
	}
	
	public ResultSet ApartmentRecommendations() throws SQLException {
		rs = Database.query("SELECT ApartmentID "
							+ "FROM APARTMENT " 
							+ "WHERE Bathrooms = " + bathroom + " AND " + "Bedrooms = " + bedroom
							+ " AND ABS(MonthRent - " + rent + ") <= 200.00 AND ABS(SquareFeet - " + sqFt + ") <= 65" + " AND ApartmentID IN(Select ApartmentID From AVAILABLE_APARTMENTS)"
							+ " Order by ABS(MonthRent - " + rent + ") Limit 4");
		
		return rs;
	} 
}//recommendations class ends


/*	
SELECT ApartmentID 
FROM APARTMENT
WHERE Bathrooms = 1 AND Bedrooms = 1 
AND ABS(MonthRent - 1040.00) <= 250.00 AND ABS(SquareFeet - 586) <= 100 And ApartmentID IN(Select ApartmentID From AVAILABLE_APARTMENTS)
Order by ABS(MonthRent - 1040.00)
Limit 3
 */
