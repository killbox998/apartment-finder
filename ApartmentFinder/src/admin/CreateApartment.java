package admin;

import java.sql.SQLException;

import database.Database;

public class CreateApartment {
	public static int create(int complexID, int apartmentNumber, String description, double deposit, double monthRent, int squareFeet, int bedrooms, int bathrooms) throws SQLException
	{
		Database.execute("INSERT INTO APARTMENT(ComplexID, Description, ApartmentNumber, Deposit, MonthRent, SquareFeet, Bedrooms, Bathrooms) VALUES("+complexID+",\'"+ Database.escapeString(description)+"\', "+apartmentNumber+", "+deposit+", "+monthRent+", "+squareFeet+", "+bedrooms+", "+bathrooms+");");
		return 0;
	}

}
