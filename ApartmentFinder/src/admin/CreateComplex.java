package admin;

import java.sql.ResultSet;
import java.sql.SQLException;

import database.Database;

public class CreateComplex {

	public static int create(String state, String city, String streetName, int streetNumber, boolean isCloseToCampus,
			boolean petsAllowed, int AmenitiesID) throws SQLException {
		//System.out.println("INSERT INTO COMPLEX(State, City, StreetName, StreetNumber, isCloseToCampus, petsAllowed, AmenitiesID) VALUES( \'"
					//	+ state + "\',\'" + city + "\',\'" + streetName + "\', " + streetNumber + ", " + petsAllowed
				//		+ ", " + AmenitiesID + ");");
		Database.execute(
				"INSERT INTO COMPLEX(State, City, StreetName, StreetNumber, isCloseToCampus, petsAllowed, AmenitiesID, IsHouse) VALUES( \'"
						+ state + "\',\'" + city + "\',\'" + streetName + "\', " + streetNumber + ", " + isCloseToCampus+","+ petsAllowed
						+ ", " + AmenitiesID + ", false);");
		ResultSet rs = Database.query("SELECT LAST_INSERT_ID()");
		rs.next();
		int ComplexId = rs.getInt(1);
		return ComplexId;
	}

}
