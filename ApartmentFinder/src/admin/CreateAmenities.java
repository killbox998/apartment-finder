package admin;

import java.sql.ResultSet;
import java.sql.SQLException;

import database.Database;

public class CreateAmenities {
	
	public static int create(LaundryType lt, ParkingType pt, FlooringType ft, boolean dishwasher, boolean airConditioner, boolean patio, boolean gym, boolean pool) throws SQLException
	{
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO AMENITIES(Laundry, Parking, Flooring, Dishwasher, AirConditioner, Patio, Gym, Pool) VALUES(");
		switch(lt)
		{
		case COMMUNAL:
			sb.append("\'Communal\',");
			break;
		case IN_UNIT:
			sb.append("\'In Unit\',");
			break;
		}
		switch(pt)
		{
		case DEDICATED_PARKING_SPOT:
			sb.append("\'Dedicated Parking spot\',");
			break;
		case PERSONAL_GARAGE:
			sb.append("\'Personal Garage\',");
			break;
		}
		switch(ft)
		{
		case HARDWOOD:
			sb.append("\'Hardwood\',");
			break;
		case TILE:
			sb.append("\'Tile\',");
			break;
		}
		
		sb.append((dishwasher ? "\'yes\',":"null,"));
		sb.append((airConditioner ? "\'yes\',":"null,"));
		sb.append((patio ? "\'yes\',":"null,"));
		sb.append((gym ? "\'yes\',":"null,"));
		sb.append((pool ? "\'yes\');":"null);"));
		//System.out.println(sb.toString());
		Database.execute(sb.toString());
		
		ResultSet rs = Database.query("SELECT LAST_INSERT_ID()");
		rs.next();
		int amenId = rs.getInt(1);
		return amenId;
	}

}
